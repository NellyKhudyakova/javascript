import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {
  transform(personsArr, filterParameter) {
    console.log(personsArr);
    if (personsArr.lenght === 0 || filterParameter === '') {
      return;
    }
    return personsArr.filter(
      function (element) {
        if(element.firstName === filterParameter || element.lastName === filterParameter){
          return element;
        } else return;
      });
  }
}
