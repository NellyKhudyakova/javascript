export class Person {
  public id: number;
  public firstName: string;
  public middleName: string;
  public lastName: string;
  public seriaP: string;
  public numberP: string;
  public email: string;
  public phone: string;

  constructor(firstName: string, lastName: string, seriaP, numberP, email, phone, middleName: string, id?: number,) {
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    this.seriaP = seriaP;
    this.numberP = numberP;
    this.email = email;
    this.phone = phone;
    this.id = id;
  }
}

// export interface PersonI {
//   id: number;
//   firstName: string;
//   middleName: string;
//   lastName: string;
//   seriaP: string;
//   numberP: string;
//   email: string;
//   phone: string;
// }
