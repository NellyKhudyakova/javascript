import {Component, OnInit} from '@angular/core';
import {Person} from "./shared/models/person.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Список людей';
  persons: Person[] = [];
  filterParameter = '';

  ngOnInit(): void {
    this.persons.push(new Person('NameA', 'LastNameA', 1111, 222222,
      'wtf@mail.ru', 1234567890, 'MiddleNameA', 1));
    this.persons.push(new Person('NameB', 'LastNameB', 3333, 444444,
      'wtf@mail.ru', 1234567890, 'MiddleNameB', 2));
  }

  onAddPerson(person) {
    // person.id = this.persons[this.persons.length - 1].id + 1;
    person.id = this.persons.length + 1;
    this.persons.push(person);
  }

  onDelete(delPerson) {
    this.persons.splice(delPerson, 1);
  }

  onEdit(arr) {
    let [index, id, fName, lName, serialP, numberP, email, phone, mName] = arr;
    let newPerson = new Person(fName, lName, serialP, numberP, email, phone, mName, id);
    this.persons.splice(index,1,newPerson);
  }

  onFilterPerson(newFilterParameter){
    this.filterParameter = newFilterParameter;
  }
}
