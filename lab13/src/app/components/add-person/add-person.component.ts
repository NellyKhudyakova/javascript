import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  personAdd: FormGroup;

  @Output() addPerson = new EventEmitter<Person>();

  constructor() { }

  ngOnInit() {
    this.personAdd = new FormGroup({
      firstName: new FormControl({value:'', disabled:false}, [Validators.required]),
      middleName: new FormControl({value:'', disabled:false}),
      lastName: new FormControl({value:'', disabled:false}, [Validators.required]),
      seriaP: new FormControl({value:'0000', disabled:false},
        [Validators.required, Validators.maxLength(4), Validators.minLength(4)]),
      numberP: new FormControl({value:'000000', disabled:false},
        [Validators.required, Validators.maxLength(6), Validators.minLength(6)]),
      email: new FormControl({value:'', disabled:false}, [ Validators.email]),
      phone: new FormControl({value:'', disabled:false})
    });
  }


  onAddPerson() {
    console.log(this.personAdd.value, typeof(this.personAdd.value) );
  let person = new Person(
    this.personAdd.value.firstName,
    this.personAdd.value.lastName,
    this.personAdd.value.seriaP,
    this.personAdd.value.numberP,
    this.personAdd.value.email,
    this.personAdd.value.phone,
    this.personAdd.value.middleName);

  //let person: PersonI = this.personAdd.value;
    this.addPerson.emit(person);
  }

}
