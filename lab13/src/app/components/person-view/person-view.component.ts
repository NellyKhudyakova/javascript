import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {

  personEdit: FormGroup;

  @Input() InPerson: Person;
  @Input() InIndex: number;
  @Output() deletePerson = new EventEmitter();
  @Output() editPerson = new EventEmitter();

  editor = false;

  constructor() { }

  ngOnInit() {
    this.personEdit = new FormGroup({
      id: new FormControl({value: this.InPerson.id, disabled:false}),
      firstName: new FormControl({value:this.InPerson.firstName, disabled:false}, [Validators.required]),
      middleName: new FormControl({value:this.InPerson.middleName, disabled:false}),
      lastName: new FormControl({value:this.InPerson.lastName, disabled:false}, [Validators.required]),
      seriaP: new FormControl({value:this.InPerson.seriaP, disabled:false},
        [Validators.required, Validators.maxLength(4), Validators.minLength(4)]),
      numberP: new FormControl({value:this.InPerson.numberP, disabled:false},
        [Validators.required, Validators.maxLength(6), Validators.minLength(6)]),
      email: new FormControl({value:this.InPerson.email, disabled:false}, [ Validators.email]),
      phone: new FormControl({value:this.InPerson.phone, disabled:false})
    });
  }

  onDelete(personIndex) {
    // let delPerson = personIndex;
    // console.log(delPerson);
    this.deletePerson.emit(personIndex);
  }

  onEdit(index) {
    // console.log([index, id, firstName, lastName]);
    this.editPerson.emit([
      index,
      this.personEdit.value.id,
      this.personEdit.value.firstName,
      this.personEdit.value.lastName,
      this.personEdit.value.seriaP,
      this.personEdit.value.numberP,
      this.personEdit.value.email,
      this.personEdit.value.phone,
      this.personEdit.value.middleName]);
    this.editor = false;
  }

}
