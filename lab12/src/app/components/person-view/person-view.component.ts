import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {
  @Input() InPerson: Person;
  @Input() InIndex: number;
  @Output() deletePerson = new EventEmitter();
  @Output() editPerson = new EventEmitter();

  editor = false;
  Pid: number;
  PFirstName: string;
  PLastName: string;

  constructor() { }

  ngOnInit() {
    this.Pid = this.InPerson.id;
    this.PFirstName = this.InPerson.firstName;
    this.PLastName = this.InPerson.lastName;
  }

  onDelete(personIndex) {
    let delPerson = personIndex;
    // console.log(delPerson);
    this.deletePerson.emit(delPerson);
  }

  onEdit(index, id, firstName, lastName) {
    // console.log([index, id, firstName, lastName]);
    this.editPerson.emit([index, id, firstName, lastName]);
    this.editor = false;
  }

}
