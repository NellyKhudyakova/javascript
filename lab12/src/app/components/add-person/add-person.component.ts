import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  firstName = '';
  lastName = '';

  @Output() addPerson = new EventEmitter<Person>();

  constructor() { }

  ngOnInit() {
  }

  onAddPerson() {
    let person = new Person(this.firstName, this.lastName);
    this.addPerson.emit(person);

  }

}
