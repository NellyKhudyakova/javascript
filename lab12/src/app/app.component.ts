import {Component, OnInit} from '@angular/core';
import {Person} from "./shared/models/person.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Список людей';
  persons: Person[] = [];

  ngOnInit(): void {
    this.persons.push(new Person('NameA', 'LastNameA', 1));
    this.persons.push(new Person('NameB', 'LastNameB', 2));
  }

  onAddPerson(person) {
    person.id = this.persons[this.persons.length - 1].id + 1;
    this.persons.push(person);
  }

  onDelete(delPerson) {
    this.persons.splice(delPerson, 1);
  }

  onEdit(arr) {
    let [PInd, newPId, newPFirstName, newPLastName] = arr;
    let newPerson = new Person(newPFirstName, newPLastName, newPId);
    this.persons.splice(PInd,1,newPerson);
  }
}
