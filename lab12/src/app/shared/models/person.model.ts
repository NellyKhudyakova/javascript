export class Person {
  public id: number;
  public firstName: string;
  public lastName: string;

  constructor(firstName: string, lastName: string, id?: number) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
  }
}
