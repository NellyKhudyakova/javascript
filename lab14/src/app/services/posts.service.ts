import { Injectable } from '@angular/core';
import {BaseApi} from "./baseApi";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Person} from "../shared/models/person.model";

@Injectable({
  providedIn: 'root'
})
export class PostsService extends BaseApi{

  options: HttpHeaders;

  constructor(public http: HttpClient) {
    super(http);
    this.options = new HttpHeaders();
    this.options = this.options.set('Content-Type', 'application/json');
  }

  async getUsers() {
    const users = await this.get('users', this.options).toPromise();
    return users.map(user => new Person(user.id, user.name, user.username, user.email, user.phone, user.address.city, user.address.street));
  }

  async postUsers(data) {
    return this.post('users', data, this.options).toPromise();
  }

  async putUsersById(id, data) {
    return this.put('users/' + id, data, this.options).toPromise();
  }

  async deleteUsersById(id) {
    return this.delete('users/' + id, this.options).toPromise();
  }
}
