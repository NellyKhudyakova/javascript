import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  personAdd: FormGroup;

  @Output() addPerson = new EventEmitter<Person>();

  constructor() { }

  ngOnInit() {
    this.personAdd = new FormGroup({
      id: new FormControl({value: '', disabled:false}),
      name: new FormControl({value:'', disabled:false}, [Validators.required]),
      username: new FormControl({value:'', disabled:false}, [Validators.required]),
      email: new FormControl({value:'', disabled:false}, [ Validators.email]),
      phone: new FormControl({value:'', disabled:false}),
      city: new FormControl({value:'', disabled:false}),
      street: new FormControl({value:'', disabled:false})
    });
  }


  onAddPerson() {
    console.log(this.personAdd.value, typeof(this.personAdd.value) );
  let person = new Person(
    this.personAdd.value.id,
    this.personAdd.value.name,
    this.personAdd.value.username,
    this.personAdd.value.email,
    this.personAdd.value.phone,
    this.personAdd.value.city,
    this.personAdd.value.street);

    this.addPerson.emit(person);
  }

}
