import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {

  personEdit: FormGroup;

  @Input() InPerson: Person;
  @Input() InIndex: number;
  @Output() deletePerson = new EventEmitter();
  @Output() editPerson = new EventEmitter();

  editor = false;

  constructor() { }

  ngOnInit() {
    this.personEdit = new FormGroup({
      id: new FormControl({value: this.InPerson.id, disabled:false}),
      name: new FormControl({value:this.InPerson.name, disabled:false}, [Validators.required]),
      username: new FormControl({value:this.InPerson.username, disabled:false}, [Validators.required]),
      email: new FormControl({value:this.InPerson.email, disabled:false}, [ Validators.email]),
      phone: new FormControl({value:this.InPerson.phone, disabled:false}),
      city: new FormControl({value:this.InPerson.address.city, disabled:false}),
      street: new FormControl({value:this.InPerson.address.street, disabled:false})
    });
  }

  onDelete(personIndex) {
    // let delPerson = personIndex;
    // console.log(delPerson);
    this.deletePerson.emit(personIndex);
  }

  onEdit(index) {
    // console.log([index, id, firstName, lastName]);
    console.log(this.personEdit.value);
    this.editPerson.emit([
      index,
      this.personEdit.value.id,
      this.personEdit.value.name,
      this.personEdit.value.username,
      this.personEdit.value.email,
      this.personEdit.value.phone,
      this.personEdit.value.city,
      this.personEdit.value.street]);
    this.editor = false;
  }
}
