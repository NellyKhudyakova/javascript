import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-filter-person',
  templateUrl: './filter-person.component.html',
  styleUrls: ['./filter-person.component.css']
})
export class FilterPersonComponent implements OnInit {

  personFilter: FormGroup;

  @Output() filterPerson = new EventEmitter<Person>();

  constructor() { }

  ngOnInit() {
    this.personFilter = new FormGroup({
      filterParameter: new FormControl({value:'', disabled:false}, [Validators.required])
    });
  }

  onFilterPerson(){
    this.filterPerson.emit(this.personFilter.value.filterParameter);
  }

}
