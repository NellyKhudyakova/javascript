export class Person {
  public id: number;
  public name: string;
  public username: string;
  public email: string;
  public phone: string;
  public address;
  public city: string;
  public street: string;

  constructor(id, name, username, email, phone, city, street) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.phone = phone;
    this.address = {city:city, street:street};
  }
}

// export interface PersonI {
//   id: number;
//   firstName: string;
//   middleName: string;
//   lastName: string;
//   seriaP: string;
//   numberP: string;
//   email: string;
//   phone: string;
// }
