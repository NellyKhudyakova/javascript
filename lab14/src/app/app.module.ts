import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { PersonViewComponent } from './components/person-view/person-view.component';
import { AddPersonComponent } from './components/add-person/add-person.component';
import { FilterPersonComponent } from './components/filter-person/filter-person.component';
import {IConfig, NgxMaskModule} from "ngx-mask";
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import {HttpClientModule} from "@angular/common/http";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    PersonViewComponent,
    AddPersonComponent,
    FilterPersonComponent,
    FilterByNamePipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
