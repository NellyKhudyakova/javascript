import {Component, OnInit} from '@angular/core';
import {Person} from "./shared/models/person.model";
import {PostsService} from "./services/posts.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Список людей';
  persons: Person[] = [];
  filterParameter = '';
  users: Person[] = [];

  constructor(private postsServise: PostsService) { }

  async ngOnInit() {
    try {
      let users = this.postsServise.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
      console.log(users);
    } catch (err) {
      console.log(err);
    }
  }

  onAddPerson(person) {
    person.id = this.users.length + 1;
    this.users.push(person);
  }

  onDelete(delPerson) {
    this.users.splice(delPerson, 1);
  }

  onEdit(arr) {
    let [index, id, name, username, email, phone, city, street] = arr;
    // let newPerson = new Person(id, name, username, email, phone, city, street);
    // this.users.splice(index,1,newPerson);
    this.users[index] = new Person(id, name, username, email, phone, city, street);
  }

  onFilterPerson(newFilterParameter){
    this.filterParameter = newFilterParameter;
  }
}
