import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MainComponent } from './components/main/main.component';
import { AddPersonComponent } from './components/add-person/add-person.component';
import { ItemEditComponent } from './components/item-edit/item-edit.component';
import { PersonViewComponent } from './components/person-view/person-view.component';
import { FilterPersonComponent } from './components/filter-person/filter-person.component';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {IConfig, NgxMaskModule} from "ngx-mask";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    AddPersonComponent,
    ItemEditComponent,
    PersonViewComponent,
    FilterPersonComponent,
    FilterByNamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
