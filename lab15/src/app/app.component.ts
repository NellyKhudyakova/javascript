import { Component } from '@angular/core';
import {isNullOrUndefined} from "util";
import {Person} from "./shared/models/person.model";
import {PostsService} from "./services/posts.service";

let initPromiseResolve;
let initPromise = new Promise(resolve => {
  initPromiseResolve = resolve;
});

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'lab15';

  users: Person[] = [];

  constructor(private postsServise: PostsService) { }

  async ngOnInit() {
    try {
      let users = this.postsServise.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
      console.log(this.users);
    } catch (err) {
      console.log(err);
    }
    initPromiseResolve();
  }

  async getUsersArr(){
    await initPromise;
    // console.log(this.users);
    return this.users;
  }
}

