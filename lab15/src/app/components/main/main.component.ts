import {Component, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
// import {PostsService} from "../../services/posts.service";
// import {isNullOrUndefined} from "util";
import {AppComponent} from "../../app.component";


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  title = 'Список людей';
  // persons: Person[] = [];
  filterParameter = '';
  usersArr: Person[] = [];

  constructor(
    private appComponent : AppComponent){}
  async ngOnInit(){
    this.usersArr = await this.appComponent.getUsersArr();
  }
  // constructor(private postsServise: PostsService) { }
  //
  // async ngOnInit() {
  //   try {
  //     let users = this.postsServise.getUsers();
  //     this.users = (isNullOrUndefined(await users)) ? [] : await users;
  //   } catch (err) {
  //     console.log(err);
  //   }
  // }

  onAddPerson(person) {
    person.id = this.usersArr.length + 1;
    this.usersArr.push(person);
  }

  onDelete(delPerson) {
    this.usersArr.splice(delPerson, 1);
  }

  onFilterPerson(newFilterParameter){
    this.filterParameter = newFilterParameter;
  }

  // getUsers(){
  //   return this.users;
  // }
}



