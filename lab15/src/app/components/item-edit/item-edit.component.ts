import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Person} from "../../shared/models/person.model";
import {AppComponent} from "../../app.component";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css']
})
export class ItemEditComponent implements OnInit {

  personEdit: FormGroup;
  usersArr: Person[] = [];
  id;

  @Output() addPerson = new EventEmitter<Person>();

  constructor(
    private activatedRouter: ActivatedRoute, // хранит url адрес и параметры
    private router: Router, // этот сервис используется для навигации
    private appComponent : AppComponent
  ) {
    this.activatedRouter.params.subscribe(param => {
      this.id = param.id;
    });
    this.personEdit = new FormGroup({
      id: new FormControl({value: '', disabled:false}),
      name: new FormControl({value:'', disabled:false}, [Validators.required]),
      username: new FormControl({value:'', disabled:false}, [Validators.required]),
      email: new FormControl({value:'', disabled:false}, [ Validators.email]),
      phone: new FormControl({value:'', disabled:false}),
      city: new FormControl({value:'', disabled:false}),
      street: new FormControl({value:'', disabled:false})
    });
  }

  async ngOnInit() {
    this.usersArr = await this.appComponent.getUsersArr();
    const userData = this.usersArr.find(person => person.id == this.id);
    this.personEdit.setValue({
      id: this.id,
      name: userData.name,
      username: userData.username,
      email: userData.email,
      phone: userData.phone,
      city: userData.address.city,
      street: userData.address.street
    });
  }

  onEditPerson() {
    const userIndex = this.usersArr.findIndex(person => person.id == this.id);
    this.usersArr[userIndex] = new Person(
      this.personEdit.value.id,
      this.personEdit.value.name,
      this.personEdit.value.username,
      this.personEdit.value.email,
      this.personEdit.value.phone,
      this.personEdit.value.city,
      this.personEdit.value.street);
    this.router.navigate([`/`]);
  }

}
