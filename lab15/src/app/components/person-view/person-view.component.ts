import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {

  @Input() InPerson: Person;
  @Input() InIndex: number;
  @Output() deletePerson = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onDelete(personIndex) {
    this.deletePerson.emit(personIndex);
  }

}
