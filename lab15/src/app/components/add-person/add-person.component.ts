import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../shared/models/person.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

import {AppComponent} from "../../app.component";

@Component({
  selector: 'add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  personAdd: FormGroup;
  usersArr: Person[] = [];

  @Output() addPerson = new EventEmitter<Person>();

  constructor(
    private activatedRouter: ActivatedRoute, // хранит url адрес и параметры
    private router: Router, // этот сервис используется для навигации
    private appComponent : AppComponent
  ) { }

  async ngOnInit() {
    this.personAdd = new FormGroup({
      id: new FormControl({value: '', disabled:false}),
      name: new FormControl({value:'', disabled:false}, [Validators.required]),
      username: new FormControl({value:'', disabled:false}, [Validators.required]),
      email: new FormControl({value:'', disabled:false}, [ Validators.email]),
      phone: new FormControl({value:'', disabled:false}),
      city: new FormControl({value:'', disabled:false}),
      street: new FormControl({value:'', disabled:false})
    });

    this.usersArr = await this.appComponent.getUsersArr();
  }


  onAddPerson() {
    let person = new Person(
      this.personAdd.value.id || this.usersArr.length + 1,
      this.personAdd.value.name,
      this.personAdd.value.username,
      this.personAdd.value.email,
      this.personAdd.value.phone,
      this.personAdd.value.city,
      this.personAdd.value.street);
    this.usersArr.push(person);
    this.router.navigate([`/`]);

  }

}
