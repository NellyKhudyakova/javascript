import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {
  transform(personsArr, filterParameter) {
    let size = Object.keys(personsArr);
    if (size.length === 0 || filterParameter === '') {
      return;
    }

    return personsArr.filter(
      function (element) {
        let [fName, lName ] = element.name.split(' ');
        if(fName === filterParameter || lName === filterParameter){
          return element;
        } else return;
      });
  }
}
