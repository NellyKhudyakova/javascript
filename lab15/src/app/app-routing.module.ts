import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./components/main/main.component";
import {AddPersonComponent} from "./components/add-person/add-person.component";
import {ItemEditComponent} from "./components/item-edit/item-edit.component";

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'add-person', component: AddPersonComponent},
  {path: 'edit/:id', component: ItemEditComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
